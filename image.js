
var im = require('imagemagick');
var path = require('path');
var Pend = require('pend');

module.exports = {

    generate_sizes: function(file, sizes, cb) {
        "use strict";
        console.error('==== paths', im.identify.path, im.convert.path);
        im.identify(file, function(err, feat) {
            if(err) {
                console.error('Failed to identify image', err);
                return cb ? cb(err, null) : false;
            }
            var pend = new Pend();
            var fname = path.basename(file);
            var dir = path.dirname(file);
            var ext = path.extname(file);
            var type = ext.substr(1);
            var bname = fname.substr(0, fname.length - ext.length);
            Object.keys(sizes).filter(function(szn) {
                var opts = sizes[szn];
                opts.srcPath = file;
                opts.dstPath = path.join(dir, bname + '_' + szn + ext);
                opts.quality = opts.quality || 0.8;
		opts.progressive = true;
                opts.format = opts.format || type;
                opts.strip = typeof opts.strip === 'undefined' ? true :false;
                pend.go(function(opts) {
                    return function(pendDone) {
                        im.resize(opts, function(err, stdout, stderr) {
			    console.error('=== stdout resize', stdout);
                            pendDone(err);
                        });
                    };
                }(opts));
            });
            pend.wait(function(err) {
                if(err) {
                    console.error('Error from image conversion', err);
                    return cb ? cb(err, null) : false;
                }
                return cb ? cb(err, null) : false;
            });
        });
    }
};

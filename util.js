
var _ = require('underscore');
var http = require('http');
var fs = require('fs');
var urlencode = require('urlencode');
var url = require('url');
var path = require('path');
var image_util = require('./image');

module.exports = function(feed) {
    "use strict";

    var b64 = function(s) {
        return (new Buffer(s)).toString('base64');
    };

    var image_data_uri = function(filename, type) {
        return 'data:image/' + type + ';base64,' + b64(fs.readFileSync(filename));
    };

    return {
        fs: fs,
        path: path,

        b64: b64,

        image_util: image_util,
        
        image_data_uri: image_data_uri,

        image_title_alt_from_post_media: function(post, title, alt) {
            var att_id = parseInt(post.attachment_id, 10);
            var media = _.first((post.media||[]).filter(function(m) { return m.ID === att_id; }));
            var name = false;
            var mime = "png";
            if(media) {
                var exc = media.post_excerpt || media.post_name;
                name = exc.toLowerCase().replace(/[^a-z]/, '-');
                title = media.post_title;
                alt = media.post_content;
                mime = media.post_mime_type.split('\/')[1].toLowerCase();
            }

            return {title: title,
                    name: name,
                    type: mime,
                    alt: alt};
        },
        
        by_tag: function(tag) {
            return feed.filter(function(it) {
                return it.tags.indexOf(tag) !== -1;
            });
        },

        by_category: function(category) {
            return feed.filter(function(it) {
                return it.categories.indexOf(category) !== -1;
            });
        },

        by_category_tag: function(category, tag) {
            return feed.filter(function(it) {
                return it.categories.indexOf(category) !== -1 &&
                    it.tags.indexOf(tag) !== -1;
            });
        },

        local_image: function(img_url, name, sizes) {
            var target_dir = path.join(process.env.TARGET_DIR,'img','local');
            var local = path.join(target_dir, name);
            if(!fs.existsSync(target_dir)) {
                fs.mkdirSync(target_dir);
            }

            function gen_sizes() {
                if(!sizes) {
                    return;
                }
                image_util.generate_sizes(local, sizes);
            }
            
            var headers = {};
            if(fs.existsSync(local)) {
                var s = fs.statSync(local);
                var mdate = new Date(s.mtime);
                var diff = Date.now() - mdate.getTime();
                if(diff < 60*1000*5) {
                    gen_sizes();
                    return 'img/local/'+name;
                }
                headers['If-Modified-Since'] = mdate.toUTCString();
            }

            // decompose and recompose the img_url, urlencoding the file name, but not the path
            var options = url.parse(img_url);
            img_url = options.protocol + '//' + options.host + path.dirname(options.pathname) + '/' + urlencode(path.basename(options.pathname));
            options = url.parse(img_url);
            options.headers = headers;

            http.get(options, function(res) {
                if(res.statusCode === 200) {
                    var os = fs.createWriteStream(local);
                    res.pipe(os);
                    console.error('%s cached as local image %s', img_url, local);
                    os.on('close', function() {
                        gen_sizes();
                    });

                } else if(res.statusCode === 304) {
                    fs.open(local, 'r', function(err, fd) {
                        if(err) {throw err;}

                        var now = Date.now();
                        fs.futimes(fd, now, now, function(err) { // atime+mtime set to now
                            if(err) {throw err;}

                            // Access Time and Modified Time for /myfile.txt are now updated

                            fs.close(fd);
                        });
                    });
                    console.error('%s not modified, using cached local image %s', img_url, local);
                    gen_sizes();

                } else {
                    console.error('ERROR: %s returned HTTP code %d', img_url, res.statusCode);
                }
            }).on('error', (e) => {
                console.error('ERROR: Failed to fetch external image url: %s', e.message, url);
            });
            return 'img/local/'+name;
        },

        /**
         * Get a thumbnail from a post. Call with either just the post to get the smallest,
         * or with a specific size to get that size.
         * @returns Object {size: <int>, url: <string>}
         */
        thumbnail: function(post, size) {
            if(!post) {
                console.error('no post in util.thumbnail()');
                return false;
            }
            if(!post.thumbnail) {
                console.error('no thumbnail for post', post.id);
                return false;
            }
            // size-sorted list of thumbnails
            var li = post.thumbnail.split(/,/).map(function(e) { return e.trim().split(/\ /);})
                .map(function(a) {
                    a[1] = parseInt(a[1], 10);
                    return a;
                })
                .sort(function(a,b) {
                    return b[1] - a[1];
                });

            console.error('thumbnail list', li.map(function(l) { return l[1]; }));
            var obj = {};
            _.each(li, function(e) {
                obj[e[1]] = e[0];
            });

            return size && obj[size] ? {url: obj[size], size: size} :
            {url: li[0][0], size: li[0][1],
             large: li[0][0], large_size: li[0][1],
             small: li[li.length-1][0], small_size: li[li.length-1][1]};
        }
    };
};

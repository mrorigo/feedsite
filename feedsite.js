#!/usr/bin/env nodejs

var fs = require('fs');
var path = require('path');
var url = require('url');
var urlencode = require('urlencode');
var http = require('http');
var _ = require('underscore');
var parser = require('rss-parser');

((function() {
    "use strict";

    var urlOrPath = process.argv[2];
    var htmlFile = process.argv[3];

    var util = require('./util');

    var ot = _.template;
    _.template = function(text, settings) {
        var t = ot.apply(_, [text, settings]);
        return function(o) {
            o.util = util(o.feed || []);
            o.include = include;
            o.include = include;
            o.fetch = fetch;
            o.env = process.env;
            return t.apply(this, [o]);
        };
    };

    function include(f) {
        console.error('including %s', f);
        return fs.readFileSync(f).toString('utf-8');
    }
    
    function run(parsed, htmlFile, cb) {
        fs.readFile(htmlFile, function(err, html) {
            if(err) {
                return cb('Failed to read html file ' + htmlFile + ':' + err.message);
            }
            var res = false;
            try {
                console.error('running template for %s', htmlFile);
                res = _.template(html.toString())({feed: parsed});
            } catch(e) {
                console.error('failed to generate for %s', htmlFile, e, e.stack);
                process.exit(3);
            }

            cb(null, res);
        });
    }

    function handle_res(res, htmlFile, cb) {

        // consume response body
        var parsed = false;
        try {
            parsed = JSON.parse(res);
            run(parsed, htmlFile, cb);
        } catch(e) {
            console.error('failed to parse json', e);
            // Might be xml ?
            parser.parseString(urlOrPath, function(err, parsed) {
                if(err) {
                    return cb('Failed to parse XML:' + err);
                }
                run(parsed.feed, htmlFile, cb);
            });
        }
    }

    function http_cache_get(feed_url, cb) {
        var target_dir = 'feed_cache';
        var name = "feed_" + feed_url.replace(/[^a-z0-9]/g, '_');
        var local = path.join(target_dir, name);
        if(!fs.existsSync(target_dir)) {
            fs.mkdirSync(target_dir);
        }

        var headers = {};
        if(fs.existsSync(local)) {
            var s = fs.statSync(local);
            var mdate = new Date(s.mtime);
            var diff = Date.now() - mdate.getTime();
            if(diff > 0 && diff < 60*1000) {
                return cb(local);
            }
            headers['If-Modified-Since'] = mdate.toUTCString();
        }

        var options = url.parse(feed_url);
        options.headers = headers;
        http.get(options, function(res) {
            if(res.statusCode === 200) {
                var file = fs.createWriteStream(local);
                res.pipe(file);
                file.on('close', function() {
                    console.error('%s cached as %s', feed_url, local, arguments);
                    cb(local);
                });
                res.resume();
            } else if(res.statusCode === 304) {
                console.error('%s not modified, using cached feed %s', feed_url, local);
                fs.open(local, 'r', function(err, fd) {
                    if(err) {throw err;}

                    var now = Date.now();
                    fs.futimes(fd, now, now, function(err) {
                        if(err) {throw err;}

                        // Access Time and Modified Time for /myfile.txt are now updated

                        fs.close(fd);
                        cb(local);
                    });
                });
                
            } else {
                console.error('ERROR: %s returned HTTP code %d', feed_url, res.statusCode);
            }
        }).on('error', (e) => {
            console.error('ERROR: Failed to fetch url: %s', e.message, feed_url);
        });
    }

    function fetch(urlOrPath, htmlFile, cb) {
        
        if(urlOrPath.startsWith('http')) {
            http_cache_get(urlOrPath, function(local_file) {
                handle_res(fs.readFileSync(local_file), htmlFile, cb);
            });
        } else {
            handle_res(fs.readFileSync(urlOrPath).toString(), htmlFile, cb);
        }
    }

    fetch(urlOrPath, htmlFile, function(err, res) {
        if(err) {
            console.error('Failed:' + err);
            process.exit(1);
        }
        console.log(res);
    });

})());

# README #

Generate pages using RSS feed and underscore templates.

## Setup

    # git clone https://mrorigo@bitbucket.org/mrorigo/feedsite.git
    # cd feedsite
    # npm install

## Usage

Run from the command line:

    # cd /path/to/feedsite.git/
    # ./feedsite.js http://rss.slashdot.org/Slashdot/slashdotMainatom test.html
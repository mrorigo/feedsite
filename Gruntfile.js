module.exports = function(grunt) {
    "use strict";

    grunt.initConfig({
        pkg: grunt.file.readJSON('./package.json'),

        bump: {
            options: {
                updateConfigs: ['pkg'],
                pushTo: 'origin',
                gitDescribeOptions: '--tags --always'
            }
        }
    });     

    grunt.loadNpmTasks('grunt-bump');

};
